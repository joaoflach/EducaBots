// App.js

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, StatusBar } from "react-native";
import { createStackNavigator } from "react-navigation";
import Chat from "./screens/Chat";
import Home from "./screens/Home";
import Info from "./screens/Info";
import Feedback from "./screens/Feedback";
import Bots from "./screens/Bots";
import Color from "react-native-material-color";
import { Asset } from "expo";
Asset;
const AppNavigator = createStackNavigator({
  HomeScreen: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      //title: "EducaBots",
      header: null
      //headerStyle: {
      //    backgroundColor: Color.Green,
      //},
      //headerTintColor: '#fff',
      //headerTitleStyle: {
      //    fontWeight: 'bold',
      //},
    })
  },
  InfoScreen: {
    screen: Info,
    navigationOptions: ({ navigation }) => ({
      title: "Sobre o EducaBots",
      headerStyle: {
        backgroundColor: Color.Green
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  },
  FeedbackScreen: {
    screen: Feedback,
    navigationOptions: ({ navigation }) => ({
      title: "Feedback",
      headerStyle: {
        backgroundColor: Color.Green
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  },
  BotsScreen: {
    screen: Bots,
    navigationOptions: ({ navigation }) => ({
      title: "Bots Disponíveis",
      headerStyle: {
        backgroundColor: Color.Green
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  },
  ChatScreen: {
    screen: Chat,
    navigationOptions: ({ navigation }) => ({
      //title: "Sobre o EducaBots",
      headerStyle: {
        backgroundColor: Color.Green
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  }
});

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <AppNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  titulo: {
    //marginTop: Dimensions.get('window').height/3,
    //marginTop: 0,
    fontSize: 28,
    color: "#3CDE4F"
  }
});
