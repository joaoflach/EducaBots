// Home.js

import React, { Component } from "react";
import {
  Platform,
  WebView,
  ScrollView,
  TouchableHighlight,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Button,
  View,
  Text
} from "react-native";
import { Icon } from "react-native-elements";

var information =
  '\
<!DOCTYPE html>\
<html>\
    <body>\
        <h3>O que é?</h3>\
	      <ul>\
            <p>O EducaBots é um aplicativo que possibilita a você conversar com o conteúdo de diversas áreas do conhecimento, fazendo com que você aprenda de forma interativa, por meio da comunicação. </p>\
        </ul>\
            <h3>Equipe</h3>\
	          <ul>\
            <p>Somos uma equipe de pesquisadores de Informática na Educação (PPGIE/UFRGS), e estamos inseridos no contexto do Projeto AVATAR, financiado pela CAPES 2017/2018. </p>\
	      </ul>\
                    <h3>Contato</h3>\
	                      <ul>\
	                          <li><h4>Coordenação do projeto</h4></li>\
	                          <p>- Liane Margarida Rockenbach Tarouco <BR> liane@penta.ufrgs.br</p>\
	                          <p>- Aliane Loureiro Krassmann <BR> alkrassmann@gmail.com</p>\
	                          <li><h4>Desenvolvimento</h4></li>\
	                          <p>- João Flach <BR> joaoflach@gmail.com</p>\
	                      </ul>\
\
                        <h3>Código do projeto</h3>\
	                      <ul>\
	                          <li><a href="https://gitlab.com/joaoflach/EducaBots">Gitlab</a></li>\
	                      </ul>\
    </body>\
</html>\
';

export class Info extends Component {
  save_file = () => {
    Expo.FileSystem.writeAsStringAsync(
      Expo.FileSystem.documentDirectory + "info.html",
      information
    );
  };
  render() {
    this.save_file();
    return (
      <WebView
        style={styles.web_response}
        source={{ uri: "https://docs.google.com/forms/d/e/1FAIpQLSdn1G1e1hPSmeMYM3eSAStO_XofWPRlpkW1voGisWgIIVUlig/viewform?usp=sf_link" }}
      />
    );
  }
}

export default Info;

const styles = StyleSheet.create({
  sobre_style: {
    margin: 30
  },
  web_response: {
    //marginTop: Dimensions.get('window').height/3,
    marginTop: 0,
    marginBottom: 10,
    marginLeft: 0,
    marginRight: 0
  }
});
