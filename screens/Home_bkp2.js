import React, { Component } from "react";
import {
  ScrollView,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  StyleSheet,
  View
} from "react-native";

import Modal from "react-native-modal";
import Grid from "react-native-grid-component";
import Color from "react-native-material-color";
import { Icon } from "react-native-elements";

var width_device = Dimensions.get("window").width;

var image_button_height = width_device / 2;

var conhecimentos = require("../assets/txt/conhecimentos.json");
var string_inicial_conhecimentos = "Conhecimento do bot:\n\n";

var metis_img = require("../assets/images/metis.png");
var atena_img = require("../assets/images/atena.png");
var jimmy_img = require("../assets/images/estevam.png");

var string_inicial_metis =
  "Oi, eu sou a METIS (Mediadora de Educação em Tecnologia Informática e Socializadora). Estou por dentro de muitas tecnologias e vou te ajudar a utilizá-las. Esses são os assuntos sobre os quais sei conversar: \n\n";
var string_inicial_atena =
  "Oi, eu sou a ATENA (Agente Tutor para Ensino e Navegação no Ambiente). Nasci em um mundo virtual de Física. Esses são os assuntos sobre os quais sei conversar:\n\n";
var string_inicial_jimmy =
  "Oi, eu sou o ESTEVAM (EStagiário TEcnico VirtuAl em Matemática financeira). Gosto muito de aprender sobre o mundo financeiro, adoro economia. Esses são os assuntos sobre os quais sei conversar:\n\n";
var string_inicial_escriba =
  "Oi, eu sou o ESCRIBA (Especialista no Suporte à CRIação de Bagagem  Acadêmica). Vou te ajudar a escrever seu projeto. Esses são os assuntos sobre os quais sei conversar:\n\n";

export class Home extends Component {
  state = {
    modalVisible: false,
    modalBot: "atena"
  };

  render_image_button = (data, i) => (
    <TouchableOpacity style={styles.item} onPress={data.target_fun} key={i}>
      <Image style={styles.image_button} source={data.image_source} />
    </TouchableOpacity>
  );

  render_default = i => <View style={styles.item} key={i} />;

  goto_chat = chatbot_area => {
    this.setModalVisible(!this.state.modalVisible);
    const { navigate } = this.props.navigation;
    navigate("ChatScreen", { category: chatbot_area });
  };

  goto_fisica = this.goto_chat.bind(this, "fisica");
  goto_midias = this.goto_chat.bind(this, "midias");
  goto_math = this.goto_chat.bind(this, "matematica");

  stringly_bot = conhecimento_bot => {
    //JSON.stringify(conhecimentos.atena);
    var x = string_inicial_conhecimentos;

    for (i = 1; i < conhecimento_bot.length; i++) {
      x += "- " + conhecimento_bot[i] + "\n";
    }
    return x;
  };

  stringly_atena = () => {
    //JSON.stringify(conhecimentos.atena);
    var x = string_inicial_conhecimentos;
    for (i = 1; i < conhecimentos.atena.length; i++) {
      x += "- " + conhecimentos.atena[i] + "\n";
    }
    return x;
  };
  stringly_metis = () => {
    //JSON.stringify(conhecimentos.atena);
    var x = string_inicial_conhecimentos;
    for (i = 1; i < conhecimentos.metis.length; i++) {
      x += "- " + conhecimentos.metis[i] + "\n";
    }
    return x;
  };
  stringly_jimmy = () => {
    //JSON.stringify(conhecimentos.atena);
    var x = string_inicial_conhecimentos;
    for (i = 0; i < conhecimentos.jimmy.length; i++) {
      x += "- " + conhecimentos.jimmy[i] + "\n";
    }
    return x;
  };

  render_info = bot => {
    if (bot == "atena") {
      string_inicial_conhecimentos = string_inicial_atena;
      conhecimento_atual = conhecimentos.atena;
      goto_function = this.goto_fisica;
      bot_img = atena_img;
    }
    if (bot == "jimmy") {
      string_inicial_conhecimentos = string_inicial_jimmy;
      conhecimento_atual = conhecimentos.jimmy;
      goto_function = this.goto_math;
      bot_img = jimmy_img;
    }
    if (bot == "metis") {
      string_inicial_conhecimentos = string_inicial_metis;
      conhecimento_atual = conhecimentos.metis;
      goto_function = this.goto_midias;
      bot_img = metis_img;
    }

    return (
      <View style={styles.modalContent}>
        <ScrollView vertical={true}>
          <View style={styles.img_container}>
            <Image style={styles.bot_image} source={bot_img} />
          </View>
          <Text>{this.stringly_bot(conhecimento_atual)}</Text>
        </ScrollView>
        <View style={styles.input_line}>
          <Icon
            iconStyle={{ marginTop: 0 }}
            reverse
            onPress={() => {
              this.setModalVisible(!this.state.modalVisible);
            }}
            name="close"
            type="MaterialIcons"
            color="#4CD16E"
            size={20}
          />
          <TouchableOpacity style={styles.button} onPress={goto_function}>
            <Text style={styles.button_text}> Conversar com o bot </Text>
          </TouchableOpacity>
          {/*<Icon
                iconStyle={{marginTop: 0}}
                reverse
                onPress={goto_function}
                name='arrow-forward'
                type='MaterialIcons'
                color='#4CD16E'
                size={20}
                      />*/}
        </View>
      </View>
    );
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render_modal(visible, bot) {
    this.setState({ modalBot: bot });
    this.setState({ modalVisible: visible });
  }

  render_modal_math = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "jimmy"
  );
  render_modal_midias = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "metis"
  );
  render_modal_fisica = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "atena"
  );

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Color.GREY[100] }}>
        <Text style={styles.title}>BOTS DISPONÍVEIS:</Text>

        <Grid
          style={styles.list}
          renderItem={this.render_image_button}
          renderPlaceholder={this._render_default}
          data={[
            {
              image_source: require("../assets/images/fisica_text.png"),
              target_fun: this.render_modal_fisica
            },
            {
              image_source: require("../assets/images/mat_financeira_text.png"),
              target_fun: this.render_modal_math
            },
            {
              image_source: require("../assets/images/metodologia_text.png"),
              target_fun: this.render_modal_midias
            },
            {
              image_source: require("../assets/images/midias_text.png"),
              target_fun: this.render_modal_midias
            }
          ]}
          itemsPerRow={2}
        />

        <Modal isVisible={this.state.modalVisible}>
          {this.render_info(this.state.modalBot)}
        </Modal>
      </View>
    );
  }
}
export default Home;

const styles = StyleSheet.create({
  item: {
    flex: 1,
    height: image_button_height,
    margin: 1
  },
  list: {
    flex: 1
  },
  image_button: {
    flex: 1,
    height: image_button_height,
    width: image_button_height
  },
  modalContent: {
    backgroundColor: "white",
    padding: 15,
    justifyContent: "center",
    //alignItems: 'center',
    borderRadius: 5,
    borderColor: "rgba(0, 0, 0, 0.1)",
    maxHeight: Dimensions.get("window").height - 80
  },
  input_line: {
    //flex: 1,
    flexDirection: "row"
    //justifyContent: 'space-between'
  },
  img_container: {
    justifyContent: "center",
    alignItems: "center"
  },
  bot_image: {
    padding: 0,
    margin: 20,
    width: 100,
    height: 150
  },
  button: {
    margin: 10,
    marginHorizontal: 40,
    alignItems: "center",
    backgroundColor: Color.GREEN[700],
    padding: 10,
    borderRadius: 5
    //width: Dimensions.get('window').width-120,
  },
  title: {
    color: Color.BLACK[500],
    margin: 20,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 16
  },
  button_text: { color: "#ffffff", fontSize: 18 }
});
