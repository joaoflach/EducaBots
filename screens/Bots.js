import React, { Component } from "react";
import {
  WebView,
  ScrollView,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
} from "react-native";

import Modal from "react-native-modal";
import Grid from "react-native-grid-component";
import Color from "react-native-material-color";
import { Icon } from "react-native-elements";

var width_device = Dimensions.get("window").width;

var relation_screen = Dimensions.get("window").width / Dimensions.get("window").height;

var image_button_height = width_device / 2 - 10;

var conhecimentos = require("../assets/txt/conhecimentos.json");
var string_inicial_conhecimentos = "Conhecimento do bot:\n\n";

var metis_img = require("../assets/images/metis.png");
var atena_img = require("../assets/images/atena.png");
var jimmy_img = require("../assets/images/estevam.png");
var escriba_img = require("../assets/images/escriba.png");

var string_inicial_metis =
  "Oi, eu sou a METIS (Mediadora de Educação em Tecnologia Informática e Socializadora). Estou por dentro de muitas tecnologias e vou te ajudar a utilizá-las. Esses são os assuntos sobre os quais sei conversar: \n";
var string_inicial_atena =
  "Oi, eu sou a ATENA (Agente Tutor para Ensino e Navegação no Ambiente). Nasci em um mundo virtual de Física. Esses são os assuntos sobre os quais sei conversar:\n";
var string_inicial_jimmy =
  "Oi, eu sou o ESTEVAM (EStagiário TEcnico VirtuAl em Matemática financeira). Gosto muito de aprender sobre o mundo financeiro, adoro economia. Esses são os assuntos sobre os quais sei conversar:\n";
var string_inicial_escriba =
  "Oi, eu sou o ESCRIBA (Especialista no Suporte à CRIação de Bagagem  Acadêmica). Vou te ajudar a escrever seu projeto. Esses são os assuntos sobre os quais sei conversar:\n";

export class Bots extends Component {
  state = {
    modalVisible: false,
    modalBot: "atena"
  };

  render_image_button = (data, i) => (
    <TouchableOpacity style={styles.item} onPress={data.target_fun} key={i}>
      <Image style={styles.image_button} source={data.image_source} />
    </TouchableOpacity>
  );

  render_default = i => <View style={styles.item} key={i} />;

  goto_chat = chatbot_area => {
    this.setModalVisible(!this.state.modalVisible);
    const { navigate } = this.props.navigation;
    navigate("ChatScreen", { category: chatbot_area });
  };

  goto_fisica = this.goto_chat.bind(this, "fisica");
  goto_midias = this.goto_chat.bind(this, "midias");
  goto_math = this.goto_chat.bind(this, "matematica");
  goto_metodologia = this.goto_chat.bind(this, "metodologia");

  stringly_bot = conhecimento_bot => {
    //JSON.stringify(conhecimentos.atena);
    var string_conhecimento = {
      init: string_inicial_conhecimentos,
      left: "",
      right: ""
    };

    var tam = conhecimento_bot.length;

    for (i = 0; i < tam; i++) {
      if (i <= tam / 2) {
        string_conhecimento.left += "- " + conhecimento_bot[i] + "\n";
      } else {
        string_conhecimento.right += "- " + conhecimento_bot[i] + "\n";
      }
    }

    return string_conhecimento;
  };

  render_info = bot => {
    if (bot == "atena") {
      string_inicial_conhecimentos = string_inicial_atena;
      conhecimento_atual = conhecimentos.atena;
      goto_function = this.goto_fisica;
      bot_img = atena_img;
    }
    if (bot == "jimmy") {
      string_inicial_conhecimentos = string_inicial_jimmy;
      conhecimento_atual = conhecimentos.jimmy;
      goto_function = this.goto_math;
      bot_img = jimmy_img;
    }
    if (bot == "metis") {
      string_inicial_conhecimentos = string_inicial_metis;
      conhecimento_atual = conhecimentos.metis;
      goto_function = this.goto_midias;
      bot_img = metis_img;
    }
    if (bot == "escriba") {
      string_inicial_conhecimentos = string_inicial_escriba;
      conhecimento_atual = conhecimentos.escriba;
      goto_function = this.goto_metodologia;
      bot_img = escriba_img;
    }

    return (
      <View style={styles.modalContent}>
        <ScrollView vertical={true} style={{ marginTop: 0, flex: 1 }}>
          <View style={styles.img_container}>
            <Image style={styles.bot_image} source={bot_img} />
          </View>
          <Text style={styles.lista_conhecimento}>
            {this.stringly_bot(conhecimento_atual).init}
          </Text>
          {/*<View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', flex: 1, margin: 20,}}>*/}
          <View>
            <Text style={styles.lista_conhecimento}>
              {this.stringly_bot(conhecimento_atual).left}
              {this.stringly_bot(conhecimento_atual).right}
            </Text>
          </View>
          {/*<View>
                    <Text style={styles.lista_conhecimento}>{this.stringly_bot(conhecimento_atual).right}</Text>
                    </View>
                    </View>*/}
        </ScrollView>

        <View style={styles.input_line}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.setModalVisible(!this.state.modalVisible);
            }}
          >
            <Text style={styles.button_text}> Voltar </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={goto_function}>
            <Text style={styles.button_text}> Conversar</Text>
          </TouchableOpacity>

          {/*<Icon
                iconStyle={{marginTop: 0}}
                reverse
                onPress={goto_function}
                name='arrow-forward'
                type='MaterialIcons'
                color='#4CD16E'
                size={20}
                      />*/}
        </View>
        {/*<View
          style={{
            ...StyleSheet.absoluteFillObject,
            alignSelf: "flex-end",
            top: 5,
            right: 5,
            position: "absolute"
          }}
        >
          <Icon
                iconStyle={{marginTop: 0}}
                reverse
                onPress={() => {this.setModalVisible(!this.state.modalVisible);}}
                name='close'
                type='MaterialIcons'
                color='#FF2222'
                size={20}
                      />
        </View>*/}
      </View>
    );
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render_modal(visible, bot) {
    this.setState({ modalBot: bot });
    this.setState({ modalVisible: visible });
  }

  render_modal_math = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "jimmy"
  );
  render_modal_midias = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "metis"
  );
  render_modal_fisica = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "atena"
  );
  render_modal_metodologia = this.render_modal.bind(
    this,
    !this.state.modalVisible,
    "escriba"
  );

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Color.GREY[100], margin: 10 }}>
        <Grid
          style={styles.list}
          renderItem={this.render_image_button}
          renderPlaceholder={this._render_default}
          data={[
            {
              image_source: require("../assets/images/fisica_text.png"),
              target_fun: this.render_modal_fisica
            },
            {
              image_source: require("../assets/images/mat_financeira_text.png"),
              target_fun: this.render_modal_math
            },
            {
              image_source: require("../assets/images/metodologia_text.png"),
              target_fun: this.render_modal_metodologia
            },
            {
              image_source: require("../assets/images/midias_text.png"),
              target_fun: this.render_modal_midias
            }
          ]}
          itemsPerRow={2}
        />

        <Modal isVisible={this.state.modalVisible}>
          {this.render_info(this.state.modalBot)}
        </Modal>
      </View>
    );
  }
}
export default Bots;

const styles = StyleSheet.create({
  item: {
    flex: 1,
    height: image_button_height,
    margin: 1
  },
  list: {
    flex: 1
  },
  image_button: {
    flex: 1,
    height: image_button_height,
    width: image_button_height
  },
  modalContent: {
    backgroundColor: "white",
    padding: 15,
    justifyContent: "center",
    //alignItems: 'center',
    borderRadius: 5,
    borderColor: "rgba(0, 0, 0, 0.1)",
    maxHeight: Dimensions.get("window").height - 80,
    flex: 1
  },
  input_line: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  img_container: {
      flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    //backgroundColor: '#450000',
    height: Dimensions.get("window").height * relation_screen * 0.4,
  },
  bot_image: {
    padding: 0,
    //margin: 20,
    //width: 100,
    //height: 150,
    //width: Dimensions.get("window").width * relation_screen * 0.8,
    //height: Dimensions.get("window").height * relation_screen * 0.5
    flex: 1,
    aspectRatio: 1,
    resizeMode: 'contain',
    //backgroundColor: '#345424'
  },
  button: {
    margin: 10,
    //marginHorizontal: 40,
    alignItems: "center",
    backgroundColor: Color.Green,
    padding: 10,
    borderRadius: 5,
    width: Dimensions.get("window").width / 3
  },
  title: {
    color: Color.BLACK[500],
    margin: 20,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 16
  },
  button_text: { color: "#ffffff", fontSize: 18 },
  lista_conhecimento: {
    fontSize: 14,
    margin: 10
  },
  web_response: {
    //marginTop: Dimensions.get('window').height/3,
    marginTop: 0,
    marginBottom: 10,
    marginLeft: 0,
    marginRight: 0
  }
});
