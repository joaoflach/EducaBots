// Chat.js

import React, { Component } from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
  Dimensions,
  TextInput,
  Button,
  ActivityIndicator,
  TouchableHighlight,
  Alert,
  Keyboard,
} from "react-native";
import { Header } from "react-navigation"
import { WebBrowser } from "expo";
import { Icon } from "react-native-elements";
import AnimatedEllipsis from "react-native-animated-ellipsis";
import Color from "react-native-material-color";
import Modal from "react-native-modal";

var response;
var posts;
var final;
var question_url;
var chatbot = "atenaweb";
var chatbot_name = "";
var question_string = "olá";
var first = true;
var msg_init = new Object();
msg_init.botsay = "Bem vindo estudante!";
var relation_screen = Dimensions.get("window").width / Dimensions.get("window").height;
var font_size = 30 + Dimensions.get("window").width * 0.2;

var require_stevam = require("../assets/images/estevam.png");

var conhecimentos = require("../assets/txt/conhecimentos.json");
var string_inicial_conhecimentos = "Conhecimentos do bot:\n\n";

let _this = null;


export class Chat extends Component {


  static navigationOptions = ({ navigation }) => ({
      headerBackground: (
         <Image
         style={{
               justifyContent: 'center',
               alignItems: 'center',
               flex: 1,
               width: Dimensions.get("window").width + 10,
            }}
           source={navigation.state.params.header_background}
         />
       ),
    title: navigation.state.params.title || "default title",
    headerRight: navigation.state.params && navigation.state.params.headerRight
  });

  states = {
    inputText: ""
  };

  state = {
    loading: true,
    error: false,
    posts: [],
    uniqueValue: 1,
    uri: require("../assets/images/metis.png"),
    input_question: "Useless Placeholder",
    modalVisible: false,
    modalBot: "atena"
  };



  componentDidMount() {
      this.props.navigation.setParams({
          headerRight: (
              <View style={{marginRight: 15, marginTop: -20, alignItems:"center"}}>
              <Icon
                name="info"
                type="MaterialIcons"
                color={Color.White}
                onPress={this.render_modal_bot}
                size={30 + Dimensions.get("window").width / 30}
              />
              </View>
          )
    })


     }


// Modal


stringly_bot = conhecimento_bot => {
  //JSON.stringify(conhecimentos.atena);
  var string_conhecimento = {
    init: string_inicial_conhecimentos,
    left: "",
    right: ""
  };

  var tam = conhecimento_bot.length;

  for (i = 0; i < tam; i++) {
    if (i <= tam / 2) {
      string_conhecimento.left += "- " + conhecimento_bot[i] + "\n";
    } else {
      string_conhecimento.right += "- " + conhecimento_bot[i] + "\n";
    }
  }

  return string_conhecimento;
};

render_info = bot => {
  if (bot == "atena") {
    //string_inicial_conhecimentos = string_inicial_atena;
    conhecimento_atual = conhecimentos.atena;
    goto_function = this.goto_fisica;
    //bot_img = atena_img;
  }
  if (bot == "jimmy") {
    //string_inicial_conhecimentos = string_inicial_jimmy;
    conhecimento_atual = conhecimentos.jimmy;
    goto_function = this.goto_math;
    //bot_img = jimmy_img;
  }
  if (bot == "metis") {
    //string_inicial_conhecimentos = string_inicial_metis;
    conhecimento_atual = conhecimentos.metis;
    goto_function = this.goto_midias;
    //bot_img = metis_img;
  }
  if (bot == "escriba") {
    //string_inicial_conhecimentos = string_inicial_escriba;
    conhecimento_atual = conhecimentos.escriba;
    goto_function = this.goto_metodologia;
    //bot_img = escriba_img;
  }

  return (
    <View style={styles.modalContent}>
      <ScrollView vertical={true} style={{ marginTop: 0, flex: 1 }}>

        <Text style={styles.lista_conhecimento}>
          {this.stringly_bot(conhecimento_atual).init}
        </Text>
        {/*<View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', flex: 1, margin: 20,}}>*/}
        <View>
          <Text style={styles.lista_conhecimento}>
            {this.stringly_bot(conhecimento_atual).left}
            {this.stringly_bot(conhecimento_atual).right}
          </Text>
        </View>
        {/*<View>
                  <Text style={styles.lista_conhecimento}>{this.stringly_bot(conhecimento_atual).right}</Text>
                  </View>
                  </View>*/}
      </ScrollView>

      <View style={styles.input_line}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <Text style={styles.button_text}> Voltar </Text>
        </TouchableOpacity>



        {/*<Icon
              iconStyle={{marginTop: 0}}
              reverse
              onPress={goto_function}
              name='arrow-forward'
              type='MaterialIcons'
              color='#4CD16E'
              size={20}
                    />
      </View>
      <View
        style={{
          ...StyleSheet.absoluteFillObject,
          alignSelf: "flex-end",
          top: 5,
          right: 5,
          position: "absolute"
        }}
      >
        <Icon
              iconStyle={{marginTop: 0}}
              reverse
              onPress={() => {this.setModalVisible(!this.state.modalVisible);}}
              name='close'
              type='MaterialIcons'
              color='#FF2222'
              size={20}
                    />*/}
      </View>
    </View>
  );
};


setModalVisible(visible) {
  this.setState({ modalVisible: visible });
}

render_modal(visible, bot) {
  this.setState({ modalBot: bot });
  this.setState({ modalVisible: visible });
}

render_modal_math = this.render_modal.bind(
  this,
  !this.state.modalVisible,
  "jimmy"
);
render_modal_midias = this.render_modal.bind(
  this,
  !this.state.modalVisible,
  "metis"
);
render_modal_fisica = this.render_modal.bind(
  this,
  !this.state.modalVisible,
  "atena"
);
render_modal_metodologia = this.render_modal.bind(
  this,
  !this.state.modalVisible,
  "escriba"
);

render_modal_bot = () => {
  if (chatbot == "atenaweb") {
    this.render_modal_fisica();
  }
  if (chatbot == "estevam") {
    this.render_modal_math();
  }
  if (chatbot == "metis") {
    this.render_modal_midias();
  }
  if (chatbot == "escriba") {
    this.render_modal_metodologia();
  }
};











  get_random = () => {
    min = 500;
    max = 2000;
    return Math.floor(Math.random() * (max - min)) + min;
  };

  choose_avatar = () => {
    console.log("state changed!");
    if (this.props.navigation.state.params.category == "fisica") {
      chatbot = "atenaweb";
      chatbot_name = "Atena";
      this.setState({
        uri: require("../assets/images/atena.png"),
        input_question: "Fale com a ATENA"
      });
      this.props.navigation.setParams({ title: "ATENA" });
      this.props.navigation.setParams({ header_background:
          require("../assets/images/fisica_background.png")});
    }
    if (this.props.navigation.state.params.category == "midias") {
      chatbot = "metis";
      chatbot_name = "Metis";
      this.setState({
        uri: require("../assets/images/metis.png"),
        input_question: "Fale com a METIS"
      });
      this.props.navigation.setParams({ title: "METIS" });
      this.props.navigation.setParams({ header_background:
          require("../assets/images/midias_background.png")});
    }
    if (this.props.navigation.state.params.category == "matematica") {
      console.log("JIMMMYYY");
      chatbot = "estevam";
      chatbot_name = "Estevam";
      this.setState({
        uri: require_stevam,
        input_question: "Fale com o ESTEVAM"
      });
      this.props.navigation.setParams({ title: "ESTEVAM" });
      this.props.navigation.setParams({ header_background:
          require("../assets/images/matematica_background.png")});
    }
    if (this.props.navigation.state.params.category == "metodologia") {
      chatbot = "escriba";
      chatbot_name = "Escriba";
      this.setState({
        uri: require("../assets/images/escriba.png"),
        input_question: "Fale com o ESCRIBA"
      });
      this.props.navigation.setParams({ title: "ESCRIBA" });
      this.props.navigation.setParams({ header_background:
          require("../assets/images/metodologia_background.png")});
    }
  };

  save_file = () => {
    Expo.FileSystem.writeAsStringAsync(
      Expo.FileSystem.documentDirectory + "resposta.html",
      final
    );
  };

  make_question_url = () => {
    console.log(chatbot);
    return (
      "http://143.54.95.244/" +
      chatbot +
      "/chatbot/conversation_start.php?say=" +
      question_string +
      "&bot_id=1&format=json&convo_id=p05o3ai449tsl1h3ee5lj1daf0"
    );
  };

  componentWillMount = () => {
    this.choose_avatar();
    first = true;
  };

  ask = async () => {
    try {
      var pergunta = this.make_question_url();
      response = await fetch(pergunta);
      posts = await response.json();

      setTimeout(() => {
        console.log("I do not leak!");
        this.setState({ loading: false, posts });
      }, this.get_random());
    } catch (e) {
      this.setState({ loading: false, error: true });
    }
  };
  forceRemount = () => {
    (async () => {
      this.setState({ loading: true });
      await this.ask();
      this.setState(({ uniqueValue }) => ({ uniqueValue: uniqueValue + 1 }));
    })();
  };
  webResponse = () => {
    const { loading, error } = this.state;

    if (first) {
      posts = msg_init;
    } else if (loading) {
      return (
        <View style={{ marginLeft: 50 }}>
          <Image
            style={styles.loading}
            source={require("../assets/images/loading.gif")}
          />
        </View>
      );
    } else if (error) {
      return (
        <View style={styles.center}>
          <Text>Conexão falhou!</Text>
        </View>
      );
    }

    return <View style={styles.container}>{this.renderResponse()}</View>;
  };

  renderResponse = () => {
    var cabecalho =
      "<!DOCTYPE html> <meta charset=\"UTF-8\"> <html> <head> <style>  \
img {\
max-width: " +
      "1000" +
      "px; width: " +
      Dimensions.get("window").width * 0.8 +
      "px;  height: auto; \
} \
iframe {\
max-width: " +
      "1000" +
      "px; width: " +
      Dimensions.get("window").width * 0.8 +
      "px; \
height:" +
      Dimensions.get("window").width * 0.5 +
      "px; \
} \
div.c {   font-size: " +
      font_size +
      '%;} \
</style> </head> <body> <div class="c">';

    var rodape = "</div></body> </html>";
    final = cabecalho.concat(posts.botsay.concat(rodape));
    final = final.replace("//www.youtube.com", "http://www.youtube.com");
    this.save_file();
    return (
      <WebView
        style={styles.web_response}
        source={{ uri: Expo.FileSystem.documentDirectory + "resposta.html" }}
      />
    );
  };

  send_question = () => {
    if (this.state.inputText != null && this.state.inputText != "") {
      Keyboard.dismiss;
      first = false;
      question_string = this.state.inputText;
      this.forceRemount();
      this.state.inputText = "";
    } else {
      Alert.alert(
        "Mensagem vazia",
        "Digite alguma coisa na caixa de texto para conversar com o chatbot",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    }
  };
  render() {
    return (
      <View style={styles.container}>
        {/*<View style={styles.titulo_box}>
              <Text style={styles.titulo}> {chatbot_name} </Text>
                </View>*/}
                <Modal isVisible={this.state.modalVisible}>
                  {this.render_info(this.state.modalBot)}
                </Modal>
        <View style={styles.img_container}>

          <Image style={styles.atenaStyle} source={this.state.uri} />

        </View>
        <View style={styles.container} key={this.state.uniqueValue}>

        <View style={styles.input_line}>
          <TextInput
            underlineColorAndroid={"transparent"}
            style={styles.inputText}
            onChangeText={text => this.setState({ inputText: text })}
            placeholder={this.state.input_question}
            clearButtonMode="while-editing"
          />
          <Icon
            reverse
            name="send"
            type="MaterialIcons"
            color={Color.Green}
            onPress={this.send_question}
            size={10 + Dimensions.get("window").width / 30}
          />

        </View>

        {/*
              <TextInput
          style={styles.inputText}
          onChangeText={(text) => this.setState({ inputText: text })}
          placeholder={this.state.input_question}
          clearButtonMode='while-editing'
              />

               <Button
          title="Enviar"
          color="#22cc55"
          onPress={this.send_question}
          style={styles.buttonsend}
                />

              <TouchableOpacity
          style={styles.buttonsend}
          onPress={this.send_question}
              >
              <Text style={styles.button_text}> ENVIAR </Text>
                </TouchableOpacity>
              <Icon
          reverse
          name='send'
          type='MaterialIcons'
          color='#4CD16E'
          onPress={this.send_question}
              />
                */}

        {this.webResponse()}
      </View>
      </View>
    );
  }
}
export default Chat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  web_response: {
    //marginTop: Dimensions.get('window').height/3,
    marginTop: 0,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10
  },
  titulo_box: {
    marginTop: 20,
    alignItems: "center"
  },
  input_line: {
    flex: 0.1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    alignSelf: "center",
    //backgroundColor: "#abc",
    margin: 10,
    //flexBasis: 50, //firstView style
    //flexBasis: 20 //secondView style
  },
  titulo: {
    //marginTop: Dimensions.get('window').height/3,
    //marginTop: 0,
    fontSize: 28,
    color: "#88cc65"
  },
  button: {
    margin: 10,
    //marginHorizontal: 40,
    alignItems: "center",
    backgroundColor: Color.Green,
    padding: 10,
    borderRadius: 5,
    width: Dimensions.get("window").width / 3
  },
  inputText: {
    height: 10 + Dimensions.get("window").height / 20,
    width: Dimensions.get("window").width * 0.7,
    //marginTop: 10,
    fontSize: 5 + Dimensions.get("window").height / 50,
    margin: 5,
    borderColor: "gray",
    borderRadius: 4,
    borderWidth: 1,
    alignSelf: "stretch",
    padding: 10
  },
  displayText: {
    margin: 40,
    fontSize: 28,
    fontWeight: "bold"
  },
  button_text: { color: "#ffffff" },
  atenaStyle: {
    padding: 0,
    marginTop: 20,
    //width: Dimensions.get("window").width / 2.5,
    //height: Dimensions.get("window").height / 4,
    flex: 1,
    //aspectRatio: 1,
    resizeMode: 'contain',
  },
  modalContent: {
    backgroundColor: "white",
    padding: 30,
    justifyContent: "center",
    //alignItems: 'center',
    borderRadius: 5,
    borderColor: "rgba(0, 0, 0, 0.1)",
    //maxHeight: Dimensions.get("window").height - 80,
    flex: 1,
  },
  img_container: {
    justifyContent: "center",
    alignItems: "center",
    //height: 200,
    //backgroundColor: '#ee4433',
    flex: 0.35,
  },
  loading: {
    width: 50,
    height: 50,
    resizeMode: "contain"
  },
  buttonsend: {
    margin: 10,
    marginHorizontal: 40,
    alignItems: "center",
    backgroundColor: Color.Green,
    padding: 10,
    borderRadius: 5
  }
});
