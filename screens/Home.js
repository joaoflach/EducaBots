// Home.js

import React, { Component } from "react";
import {
  Image,
  ScrollView,
  TouchableHighlight,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Button,
  View,
  Text
} from "react-native";
import Color from "react-native-material-color";

var width_device = Dimensions.get("window").width;

var image_button_height = width_device / 2;

var img_robot = require("../assets/images/robot.png");

//var verdao = "#4CD16E"
var verdao = "#2AB04C"

export class Home extends Component {
  goto_bots = () => {
    const { navigate } = this.props.navigation;
    navigate("BotsScreen", { category: "fisica" });
  };

  goto_info = () => {
    const { navigate } = this.props.navigation;
    navigate("InfoScreen");
  };

  goto_feedback = () => {
    const { navigate } = this.props.navigation;
    navigate("FeedbackScreen");
  };

  render() {
    return (
      <View>
        <View style={styles.title}>
          <Image style={styles.title_image} source={img_robot} />

          <Text style={styles.title_text}>Bem-vindo(a) ao EducaBots</Text>

          <TouchableOpacity style={styles.button} onPress={this.goto_bots}>
            <Text style={styles.button_text}>Chatterbots</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this.goto_info}>
            <Text style={styles.button_text}>Sobre o EducaBots</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this.goto_feedback}>
            <Text style={styles.button_text}>Sugira uma melhoria</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  general_view: {
    flex: 1
  },
  button_box: {
    flex: 1,
    marginTop: 15
  },
  button_text: {
    color: "#ffffff",
    fontSize: 20
  },
  input_line: {
    //flex: 1,
    flexDirection: "row"
    //justifyContent: 'space-between'
  },
  modal_style: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 50,
    justifyContent: "center",
    //alignItems: 'center',
    borderRadius: 5,
    borderColor: "rgba(0, 0, 0, 0.1)",
    maxHeight: Dimensions.get("window").height - 100
  },
  button: {
    //marginHorizontal: 50,
    marginVertical: 10,
    //marginLeft: 40,
    alignItems: "center",
    backgroundColor: Color.Green,
    padding: 20,
    borderRadius: 5,
    width: 100 + image_button_height * 0.8
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10
  },
  title: {
    alignItems: "center",
    marginTop: image_button_height / 3
  },
  title_text: {
    fontSize: 25,
    fontWeight: "bold",
    color: Color.Green,
    marginTop: 20,
    marginBottom: 50
  },
  title_image: {
    //flex: 1,
    height: 100 + image_button_height * 0.5,
    width: 100 + image_button_height * 0.5
  },
  two: {
    flex: 2
  }
});
